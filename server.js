var express     = require('express');
var app         = express();
var bodyParser  = require('body-parser');
var morgan      = require('morgan');
var mongoose    = require('mongoose');
// var passport	  = require('passport');
var Moment      = require('moment-timezone');
var config      = require('./config/database'); // get db config file
var User        = require('./app/models/user'); // get the mongoose model
var port 	      = process.env.PORT || 8080;
var multipart   = require('connect-multiparty');


process.env.TZ = 'America/Bogota';
// get our request parameters
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static('./images'));
// log to console
app.use(morgan('dev'));
app.use(multipart()) //Express 4

app.use(function(req, res, next) {
res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
    next();
});

// Use the passport package in our application
// app.use(passport.initialize());

// demo Route (GET http://localhost:8080)
app.get('/', function(req, res) {
  res.send('Hellos Volaires, From /');
});

mongoose.connect(config.database);
// require('./config/passport')(passport);

var apiRoutes = express.Router();


// ############################################################
//                        CRUD -GET -POST
// ############################################################

// Search User by ID
apiRoutes.post('/user_id', function(req, res) {
  User.find({user_id: req.body.user_id},function(err, user) {
    if (err) throw err;
    if (!user) {
      return res.status(403).send({success: false, msg: 'User not found.'});
    } else {
      return res.json({success: true, msg: 'User ', User: user});
    }
  });
});

// Create new user
apiRoutes.post('/signup', function(req, res) {
  if (!req.body.username || !req.body.email || !req.body.id) {
    res.json({succes: false, msg: 'Missing data.'});
  } else {
    var newUser = new User({
      username: req.body.username,
      email:  req.body.email, 
      first_name: req.body.first_name,
      last_name:  req.body.last_name,  
      gender: req.body.gender,
      brithdate: req.body.brithdate,
      avatar: req.body.avatar,
      city: req.body.city,
      id: req.body.id,
    });
    newUser.save(function(err) {
      if (err) {
        res.json({succes: false, msg: 'Username already exists.', user: newUser, error: err});
      } else {
        res.json({succes: true, msg: 'Successful created user.'});
      }
    });
  }
});

//  Update user
apiRoutes.post('/update_user', function(req, res) {  
  User.findById({id: req.body.user_id}, function(err, user) {
    if (err) throw err;
    if (!user) {
      return res.status(403).send({success: false, msg: 'User not found.'});
    } else {
      if (req.body.username)   user.username    = req.body.username;
      if (req.body.first_name) user.first_name  = req.body.first_name;
      if (req.body.last_name)  user.last_name   = req.body.last_name;
      if (req.body.gender)     user.gender      = req.body.gender;
      if (req.body.brithdate)  user.brithdate   = req.body.brithdate;
      if (req.body.avatar)     user.avatar      = req.body.avatar;
      if (req.body.city)       user.city        = req.body.city;
      if (req.body.id)         user.id          = req.body.id;
      user.save(function(err){
        if (err) throw err;
        return res.json({success: true, msg: 'User ' + user.username + ' successfully updated!'});
      });        
    }
  });
});

//Eliminar food 
apiRoutes.post('/deleted_user', function(req, res) {
  if (req.body.user_id)
    User.findOneAndRemove({id: req.body.user_id}, function(err){
      if (err) {
        res.json({succes: false, msg: 'Error deleting user!'});
      } else {
        res.json({succes: true, msg: 'Successful deleting user!'});
      }
  });
});

app.use('/api', apiRoutes);

// Start the server
app.listen(port);
console.log('Starting volaires api: http://localhost:' + port);