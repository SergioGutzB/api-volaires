var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
	username: {type: String},
	first_name: {type: String},
	last_name: {type: String},
	email: {type: String},
	brithdate: {type: Date},
	gender: {type: String},
	avatar: {type: String},
	city: {type: String},
	id: {type: String},

});

module.exports = mongoose.model('User', UserSchema);